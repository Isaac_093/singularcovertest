package isaacdeveloper.singularcovertest.home.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.detail.view.DetailAnimalFragment;
import isaacdeveloper.singularcovertest.home.IHomeZooContract;
import isaacdeveloper.singularcovertest.home.adapter.AnimalAdapter;
import isaacdeveloper.singularcovertest.home.presenter.HomeZooPresenter;
import isaacdeveloper.singularcovertest.model.Animal;


public class HomeZooFragment extends Fragment implements IHomeZooContract.IHomeZooView {

    public static final String TAG = "HomeZooFragment";

    //Views
    private RecyclerView rvAnimals;
    private FloatingActionButton fabShuffle;

    //Classes
    private ArrayList<Animal> animalsList;
    private AnimalAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private HomeZooPresenter homeZooPresenter;
    private SnapHelper snapHelper;

    public HomeZooFragment() { }

    public static HomeZooFragment newInstance() {
        HomeZooFragment fragment = new HomeZooFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home_zoo, container, false);

        initViews(rootView);
        initClasses();
        inflateViews();

        return rootView;
    }

    /**
     * En este método se pone todoo lo que tenga que ver con inicializar las vistas
     */
    @Override
    public void initViews(View v){
        rvAnimals = v.findViewById(R.id.rvAnimals);
        fabShuffle = v.findViewById(R.id.fabShuffle);
    }

    /**
     * En este método se pone todoo lo que tenga que ver con inicializar las clases/objetos
     */
    @Override
    public void initClasses(){
        homeZooPresenter = new HomeZooPresenter(this);
        animalsList = homeZooPresenter.needAnimalsFromZoo();
        mAdapter = new AnimalAdapter(this, animalsList);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        snapHelper = new LinearSnapHelper();
    }

    /**
     * En este método se pone todoo lo que tenga que ver con poner a punto las vistas
     */
    @Override
    public void inflateViews() {
        rvAnimals.setLayoutManager(mLayoutManager);
        snapHelper.attachToRecyclerView(rvAnimals);
        rvAnimals.setAdapter(mAdapter);

        fabShuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeZooPresenter.wantoToShuffleZoo(animalsList);
            }
        });
    }

    @Override
    public void showTheNewFriends() {
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Cada vez que se haga click en un item del recyclerview, se cargará el detalle de ese animal
     */
    @Override
    public void onClickItem(Object object, int id) {

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragment_container, DetailAnimalFragment.newInstance((Animal) object), DetailAnimalFragment.TAG);
        ft.addToBackStack(DetailAnimalFragment.TAG);
        ft.commit();

    }
}
