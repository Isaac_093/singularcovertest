package isaacdeveloper.singularcovertest.home.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.model.Animal;

public class AnimalHolder extends RecyclerView.ViewHolder {

    private ImageView imgAnimal;
    public TextView txtName, txtCommentsAnimal;

    public AnimalHolder(View itemView) {
        super(itemView);

        imgAnimal = itemView.findViewById(R.id.imgAnimal);
        txtName = itemView.findViewById(R.id.txtNameAnimal);
        txtCommentsAnimal = itemView.findViewById(R.id.txtCommentsAnimal);
    }

    public void onbind(Animal animal) {

        if (animal.getFriends() != null && animal.getFriends().size() > 0) {
            imgAnimal.setImageResource(animal.getHappyPhoto());
        } else {
            imgAnimal.setImageResource(animal.getSadPhoto());
        }

        txtName.setText(animal.getName());
        txtCommentsAnimal.setText(animal.getComments());

    }
}