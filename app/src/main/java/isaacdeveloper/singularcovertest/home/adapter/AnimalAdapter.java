package isaacdeveloper.singularcovertest.home.adapter;

import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.home.IHomeZooContract;
import isaacdeveloper.singularcovertest.model.Animal;

public class AnimalAdapter extends RecyclerView.Adapter<AnimalHolder> {

    private ArrayList<Animal> animalsList;
    private IHomeZooContract.IHomeZooView homeZooView;

    public AnimalAdapter(IHomeZooContract.IHomeZooView view, ArrayList<Animal> animalsSend) {
        this.homeZooView = view;
        this.animalsList = animalsSend;
    }

    @Override
    public AnimalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_animal_zoo, parent, false);

        return new AnimalHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AnimalHolder holder, final int position) {
        final Animal animal = animalsList.get(position);

        holder.onbind(animal);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeZooView.onClickItem(animal, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return animalsList.size();
    }
}
