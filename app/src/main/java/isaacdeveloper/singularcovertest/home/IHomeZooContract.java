package isaacdeveloper.singularcovertest.home;

import android.view.View;

import java.util.ArrayList;

import isaacdeveloper.singularcovertest.model.Animal;

public interface IHomeZooContract {

    interface IHomeZooView {
        void initViews(View v);
        void initClasses();
        void inflateViews();
        void onClickItem(Object object, int id);
        void showTheNewFriends();
    }

    interface IHomeZooPresenter {
        ArrayList<Animal> needAnimalsFromZoo();
        void wantoToShuffleZoo(ArrayList<Animal> animalsFromZoo);
        void animalsShuffled();
    }

    interface IHomeZooInteractor {
        ArrayList<Animal> getAnimalsFromZoo();
        void shuffleAnimalsFromZoo(ArrayList<Animal> animalsFromZoo);
    }

}
