package isaacdeveloper.singularcovertest.home.presenter;

import java.util.ArrayList;

import isaacdeveloper.singularcovertest.home.IHomeZooContract;
import isaacdeveloper.singularcovertest.home.interactor.HomeZooInteractor;
import isaacdeveloper.singularcovertest.model.Animal;

public class HomeZooPresenter implements IHomeZooContract.IHomeZooPresenter {

    private IHomeZooContract.IHomeZooView homeZooView;
    private IHomeZooContract.IHomeZooInteractor homeZooInteractor;

    public HomeZooPresenter(IHomeZooContract.IHomeZooView homeZooView) {
        this.homeZooView = homeZooView;
        this.homeZooInteractor = new HomeZooInteractor(this);
    }

    @Override
    public ArrayList<Animal> needAnimalsFromZoo() {
        return homeZooInteractor.getAnimalsFromZoo();
    }

    @Override
    public void wantoToShuffleZoo(ArrayList<Animal> animalsFromZoo) {
        homeZooInteractor.shuffleAnimalsFromZoo(animalsFromZoo);
    }

    @Override
    public void animalsShuffled() {
        homeZooView.showTheNewFriends();
    }
}
