package isaacdeveloper.singularcovertest.home.interactor;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

import isaacdeveloper.singularcovertest.home.IHomeZooContract;
import isaacdeveloper.singularcovertest.model.Animal;
import isaacdeveloper.singularcovertest.repositori.RepositoriZoo;

public class HomeZooInteractor implements IHomeZooContract.IHomeZooInteractor {

    /**
     * Los valores mínimo y máximo del valor númerico aleatorio
     * para decidir si serán amigos o no
     */
    public static final int MIN_RANDOM = 1;
    public static final int MAX_RANDOM = 100;

    private IHomeZooContract.IHomeZooPresenter splashPresenter;
    private RepositoriZoo repositoriZoo;
    private ArrayList<Animal> animalsListBeforeShuffle;

    public HomeZooInteractor(IHomeZooContract.IHomeZooPresenter splashPresenter) {
        this.splashPresenter = splashPresenter;
        this.repositoriZoo = new RepositoriZoo();
    }

    private void setAnimalsListBeforeShuffle(ArrayList<Animal> animalsListBeforeShuffle) {
        this.animalsListBeforeShuffle = animalsListBeforeShuffle;
    }

    private ArrayList<Animal> getAnimalsListBeforeShuffle() {
        return animalsListBeforeShuffle;
    }

    @Override
    public ArrayList<Animal> getAnimalsFromZoo() {
        return repositoriZoo.getAnimalsFromZoo();
    }

    @Override
    public void shuffleAnimalsFromZoo(ArrayList<Animal> animalsFromZoo) {

        //Guardamos la lista que había antes para no perder ningún animal por el camino
        setAnimalsListBeforeShuffle(animalsFromZoo);
        cleanFriends(animalsFromZoo);

        //Bucle que irá comprabando todos los animales y sus amigos
        // y las condiciones que deben cumplir para ser amigos
        for (int i = 0; i < animalsFromZoo.size(); i++) {

            Animal animalToCheck = getAnimalsListBeforeShuffle().get(i);

            for (int x = 0; x < animalsFromZoo.size(); x++) {

                Animal animalForFriend = animalsFromZoo.get(x);

                if (animalForFriend.getId() != animalToCheck.getId()) {

                    if (!checkIfIsFriend(animalToCheck, animalForFriend)) {

                        if (checkIfWillBeFriend(randomNumber())) {

                            //Aquí el animal acaba siendo amigo
                            animalToCheck.getFriends().add(animalForFriend);
                            Log.d("shuffleAnimalsFromZoo", animalToCheck.getName() + " has a new friend called " + animalForFriend.getName());

                            if (!checkIfIsFriend(animalForFriend, animalToCheck)) {

                                //Y por consecuencia el otro obtiene otro amigo
                                animalForFriend.getFriends().add(animalToCheck);
                                Log.d("shuffleAnimalsFromZoo", animalForFriend.getName() + " has a new friend called " + animalToCheck.getName());

                            }

                        }

                    }

                }

            }

        }

        splashPresenter.animalsShuffled();
    }

    /**
     * Cada vez que hagamos shuffle, todos los animales tendrán nuevos amigos
     * y perderán otros aleatoriamente
     */
    private void cleanFriends(ArrayList<Animal> animalsFromZoo) {

        for (int i = 0; i < animalsFromZoo.size(); i++) {
            animalsFromZoo.get(i).getFriends().clear();
        }

        Log.d("cleanFriends", "A day has passed and everyone has lost their friends");

    }

    /**
     * Generamos el número aleatorio que servirá para saber si el animal será amigo o no del otro
     */
    private int randomNumber() {
        Random rand = new Random();
        return rand.nextInt((MAX_RANDOM - MIN_RANDOM) + 1) + MIN_RANDOM;
    }

    /**
     * Aquí se decide si el animal será amigo o no del otro animal
     * comprobando si el número enviado es mayor a 50
     */
    private boolean checkIfWillBeFriend(int numberToCheck){

        if(numberToCheck > 50) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Aquí comprobaremos si el primer animal es amigo del segundo animal,
     * para saber si hace falta añadirlo o no a la lista de amigos del primer animal
     */
    private boolean checkIfIsFriend(Animal animaltoCheck, Animal animalFriend) {

        for (int i = 0; i < animaltoCheck.getFriends().size(); i++) {

            if (animaltoCheck.getFriends().get(i).getId() == animalFriend.getId()) {
                return true;
            }

        }

        return false;
    }
}
