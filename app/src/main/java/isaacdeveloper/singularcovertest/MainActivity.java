package isaacdeveloper.singularcovertest;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import isaacdeveloper.singularcovertest.detail.view.DetailAnimalFragment;
import isaacdeveloper.singularcovertest.home.view.HomeZooFragment;
import isaacdeveloper.singularcovertest.model.Animal;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    /**
     * En este método cargamos el primer fragment que verá el usuario
     */
    private void init() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_container, HomeZooFragment.newInstance(), HomeZooFragment.TAG);
        ft.commit();
    }
}
