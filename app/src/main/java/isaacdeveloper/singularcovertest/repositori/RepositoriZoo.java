package isaacdeveloper.singularcovertest.repositori;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import java.util.ArrayList;
import isaacdeveloper.singularcovertest.model.Animal;
import isaacdeveloper.singularcovertest.storage.SCPrefStorage;

public class RepositoriZoo extends SCPrefStorage {

    //GENERAL
    public static final String DEFAULT_EMPTY_VALUE = "";

    //FISHES
    public static final String ANIMALS_LIST = "ANIMALS_LIST";

    public RepositoriZoo() { }

    public ArrayList<Animal> getAnimalsFromZoo() {

        ArrayList<Animal> animals = new Gson().fromJson(loadData(ANIMALS_LIST, null), new TypeToken<ArrayList<Animal>>(){}.getType());

        if (animals != null && animals.size() > 0) {
            return animals;
        } else {
            return null;
        }
    }

    public void setAnimalsFromZoo(ArrayList<Animal> animalsFromZoo) {

        Gson gson = new GsonBuilder().create();
        JsonArray jsArray = gson.toJsonTree(animalsFromZoo).getAsJsonArray();

        saveData(ANIMALS_LIST, jsArray.toString());
    }

    /**
     * Método para limpiar la información
     */
    public void cleanAnimalsFromZoo(){
        setAnimalsFromZoo(null);
    }

}
