package isaacdeveloper.singularcovertest.storage;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import isaacdeveloper.singularcovertest.SCApplication;

public class SCPrefStorage {

    public static String loadData(String prefName)
    {
        return loadData(prefName, null);
    }

    public static String loadData(String prefName, String defaultValue)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SCApplication.INSTANCE.getApplicationContext());
        return preferences.getString(prefName, defaultValue);
    }

    public static void saveData(String prefName, String prefValue)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SCApplication.INSTANCE.getApplicationContext());
        SharedPreferences.Editor prefEditor = preferences.edit();
        prefEditor.putString(prefName, prefValue);
        prefEditor.commit();
    }

    public static void removeData(String prefName)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SCApplication.INSTANCE.getApplicationContext());
        SharedPreferences.Editor prefEditor = preferences.edit();
        prefEditor.remove(prefName);
        prefEditor.commit();
    }

}
