package isaacdeveloper.singularcovertest;

import android.app.Application;
import android.content.Context;

public class SCApplication extends Application {

    public static SCApplication INSTANCE;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        SCApplication.INSTANCE = this;
    }

}
