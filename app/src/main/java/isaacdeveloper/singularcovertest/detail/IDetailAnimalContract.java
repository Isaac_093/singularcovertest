package isaacdeveloper.singularcovertest.detail;

import android.view.View;

public interface IDetailAnimalContract {

    interface IDetailAnimalView {
        void initViews(View v);
        void initClasses();
        void inflateViews();
        void addValuesToViews();
    }

}
