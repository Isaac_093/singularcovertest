package isaacdeveloper.singularcovertest.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.model.Animal;

public class FriendAdapter extends RecyclerView.Adapter<FriendHolder> {

    private ArrayList<Animal> friendsList;

    public FriendAdapter(ArrayList<Animal> friendsSend) {
        this.friendsList = friendsSend;
    }

    @Override
    public FriendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_animal_friend, parent, false);
        return new FriendHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FriendHolder holder, final int position) {
        final Animal animal = friendsList.get(position);
        holder.onbind(animal);
    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }
}