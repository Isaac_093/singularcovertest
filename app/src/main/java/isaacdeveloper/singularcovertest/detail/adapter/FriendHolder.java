package isaacdeveloper.singularcovertest.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.model.Animal;

public class FriendHolder extends RecyclerView.ViewHolder {

    private ImageView imgAnimal;
    public TextView txtName, txtCommentsAnimal;

    public FriendHolder(View itemView) {
        super(itemView);

        imgAnimal = itemView.findViewById(R.id.imgFriendAnimal);
        txtName = itemView.findViewById(R.id.txtNameFriend);
    }

    public void onbind(Animal animal) {

        if (animal.getFriends() != null && animal.getFriends().size() > 0) {
            imgAnimal.setImageResource(animal.getHappyPhoto());
        } else {
            imgAnimal.setImageResource(animal.getSadPhoto());
        }

        txtName.setText(animal.getName());

    }

}