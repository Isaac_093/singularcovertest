package isaacdeveloper.singularcovertest.detail.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.detail.IDetailAnimalContract;
import isaacdeveloper.singularcovertest.detail.adapter.FriendAdapter;
import isaacdeveloper.singularcovertest.model.Animal;

public class DetailAnimalFragment extends Fragment implements IDetailAnimalContract.IDetailAnimalView {

    public static final String TAG = "DetailAnimalFragment";

    private static final String ANIMAL_TO_SHOW = "ANIMAL_TO_SHOW";

    //Views
    private ImageView imgAnimal;
    private TextView txtNameAnimal;
    private TextView txtMessageAnimal;
    private TextView txtKindAnimal;
    private TextView txtFavouriteFoodAnimal;
    private TextView txtCommentsAnimal;
    private TextView txtFriends;
    private RecyclerView rvFriends;

    //Classes
    private ArrayList<Animal> friendsList;
    private FriendAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SnapHelper snapHelper;
    private Animal animalFromZoo;

    public DetailAnimalFragment() { }

    public static DetailAnimalFragment newInstance(Animal animalSend) {
        DetailAnimalFragment fragment = new DetailAnimalFragment();
        Bundle args = new Bundle();
        args.putParcelable(ANIMAL_TO_SHOW, animalSend);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalFromZoo = getArguments().getParcelable(ANIMAL_TO_SHOW);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail_animal, container, false);

        initViews(rootView);
        initClasses();
        inflateViews();
        addValuesToViews();

        return rootView;
    }

    @Override
    public void initViews(View v) {
        rvFriends = v.findViewById(R.id.rvFriendsOfAnimal);
        imgAnimal = v.findViewById(R.id.imgAnimalDetail);
        txtCommentsAnimal = v.findViewById(R.id.txtCommentsAnimalDetail);
        txtFavouriteFoodAnimal = v.findViewById(R.id.txtFavouriteFoodAnimal);
        txtKindAnimal = v.findViewById(R.id.txtKindAnimal);
        txtMessageAnimal = v.findViewById(R.id.txtMessageAnimal);
        txtNameAnimal = v.findViewById(R.id.txtNameAnimal);
        txtFriends = v.findViewById(R.id.txtFriends);
    }

    @Override
    public void initClasses() {

        if (animalFromZoo.getFriends() != null) {
            friendsList = animalFromZoo.getFriends();
        } else {
            friendsList = new ArrayList<>();
        }

        mAdapter = new FriendAdapter(friendsList);
        mLayoutManager = new LinearLayoutManager(getContext());
        snapHelper = new LinearSnapHelper();
    }

    @Override
    public void inflateViews() {
        rvFriends.setLayoutManager(mLayoutManager);
        snapHelper.attachToRecyclerView(rvFriends);
        rvFriends.setAdapter(mAdapter);
    }

    @Override
    public void addValuesToViews() {

        if (animalFromZoo.getFriends() != null && animalFromZoo.getFriends().size() > 0) {
            imgAnimal.setImageResource(animalFromZoo.getHappyPhoto());
            txtMessageAnimal.setText(animalFromZoo.getHappyMessage());
            txtFriends.setVisibility(View.VISIBLE);
        } else {
            imgAnimal.setImageResource(animalFromZoo.getSadPhoto());
            txtMessageAnimal.setText(animalFromZoo.getSadMessage());
            txtFriends.setVisibility(View.GONE);
        }

        txtCommentsAnimal.setText(getResources().getString(R.string.comments) + " " + animalFromZoo.getComments());
        txtKindAnimal.setText(getResources().getString(R.string.kind_animal) + " " + animalFromZoo.getKind());
        txtFavouriteFoodAnimal.setText(getResources().getString(R.string.favourite_food) + " " + animalFromZoo.getFavouriteFood());
        txtNameAnimal.setText(animalFromZoo.getName());

    }
}
