package isaacdeveloper.singularcovertest.splash;

public interface ISplashContract {

    interface ISplashView {
        void init();
        void goHome();
        void showError();
    }

    interface ISplashPresenter {
        void appStarted();
    }

    interface ISplashInteractor {
        boolean checkExistZoo();
        boolean createZoo();
    }

}
