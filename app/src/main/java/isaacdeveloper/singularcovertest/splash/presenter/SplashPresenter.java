package isaacdeveloper.singularcovertest.splash.presenter;

import isaacdeveloper.singularcovertest.splash.ISplashContract;
import isaacdeveloper.singularcovertest.splash.interactor.SplashInteractor;

public class SplashPresenter implements ISplashContract.ISplashPresenter {

    private ISplashContract.ISplashView splashView;
    private ISplashContract.ISplashInteractor splashInteractor;

    public SplashPresenter(ISplashContract.ISplashView splashView) {
        this.splashView = splashView;
        this.splashInteractor = new SplashInteractor(this);
    }

    /**
     * Comprueba si existe la información del zoo,
     * si existe manda al usuario a la Home
     * si no existe crea la información y
     * luego manda al usuario al zoo si todoo ha ido bien
     */
    @Override
    public void appStarted() {

        if (splashInteractor.checkExistZoo()) {
            splashView.goHome();
        } else {
            if (splashInteractor.createZoo()) {
                splashView.goHome();
            } else {
                splashView.showError();
            }
        }

    }
}
