package isaacdeveloper.singularcovertest.splash.view;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import isaacdeveloper.singularcovertest.MainActivity;
import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.splash.ISplashContract;
import isaacdeveloper.singularcovertest.splash.presenter.SplashPresenter;

public class SplashActivity extends AppCompatActivity implements ISplashContract.ISplashView {

    private SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        init();
    }

    @Override
    public void init() {
        splashPresenter = new SplashPresenter(this);
    }

    @Override
    public void goHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Por si fallase que el usuario pueda volver a intentarlo
     */
    @Override
    public void showError() {
        Snackbar.make(getCurrentFocus(), "The zoo is closed.", Snackbar.LENGTH_LONG)
                .setAction("Open zoo", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        splashPresenter.appStarted();
                    }
                })
                .show();
    }

    /**
     * Cuando el activity se haya inicializado se llamará al presenter
     * para que haga las gestiones del principio como crear la información
     * que se usará
     */
    @Override
    protected void onStart() {
        super.onStart();
        splashPresenter.appStarted();
    }
}
