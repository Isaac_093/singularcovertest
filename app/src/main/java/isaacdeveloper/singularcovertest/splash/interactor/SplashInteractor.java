package isaacdeveloper.singularcovertest.splash.interactor;

import java.util.ArrayList;

import isaacdeveloper.singularcovertest.R;
import isaacdeveloper.singularcovertest.model.Animal;
import isaacdeveloper.singularcovertest.repositori.RepositoriZoo;
import isaacdeveloper.singularcovertest.splash.ISplashContract;

public class SplashInteractor implements ISplashContract.ISplashInteractor {

    private ISplashContract.ISplashPresenter splashPresenter;
    private RepositoriZoo repositoriZoo;

    public SplashInteractor(ISplashContract.ISplashPresenter splashPresenter) {
        this.splashPresenter = splashPresenter;
        repositoriZoo = new RepositoriZoo();
    }

    /**
     * Comprueba si existe la información del zoo
     */
    @Override
    public boolean checkExistZoo() {

        if (repositoriZoo.getAnimalsFromZoo() != null) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Creamos la información del zoo
     */
    @Override
    public boolean createZoo() {

        ArrayList<Animal> animalsFromZoo = new ArrayList<>();

        Animal gorilla = new Animal(1, "Terk", "Gorilla", "Banana",
                "Makes jokes to the public", "What jokes can we do with my new friends?",
                "I will not make any more annoying jokes, I promise...", new ArrayList<Animal>(),
                R.drawable.gorilla_happy, R.drawable.gorilla_sad);

        animalsFromZoo.add(gorilla);

        Animal lion = new Animal(2, "Simba", "Lion", "Meat",
                "He is very proud", "Have you ever seen any mane like that in your life?",
                "Just envy…", new ArrayList<Animal>(),
                R.drawable.lion_happy, R.drawable.lion_sad);

        animalsFromZoo.add(lion);

        Animal meerkat = new Animal(3, "Timon", "Meerkat", "Insects",
                " It is very cheerful", "A lot of new friends, nice, nice, can we play?!",
                "Someone hates me...", new ArrayList<Animal>(),
                R.drawable.meerkat_happy, R.drawable.meerkat_sad);

        animalsFromZoo.add(meerkat);

        Animal snake = new Animal(4, "Kaa", "Snake", "Eggs", "Sometimes silly…",
                "New friends! Will anybody lay eggs?", "Where are my friends? They are hiding?",
                new ArrayList<Animal>(), R.drawable.snake_happy, R.drawable.snake_sad);

        animalsFromZoo.add(snake);

        Animal tiger = new Animal(5, "Shere Khan", "Tiger", "Meat", "Very serious",
                "What are you looking? I have not asked for friends...",
                "Finally, quiet...", new ArrayList<Animal>(), R.drawable.tiger_happy, R.drawable.tiger_sad);

        animalsFromZoo.add(tiger);

        Animal wildpig = new Animal(6, "Pumba", "Wild pig", "Insects", "Innocent",
                "Surely those teeth are for smiling", "They don't like me...",
                new ArrayList<Animal>(), R.drawable.wildpig_happy, R.drawable.wildpig_sad);

        animalsFromZoo.add(wildpig);

        repositoriZoo.setAnimalsFromZoo(animalsFromZoo);

        return checkExistZoo();

    }
}
