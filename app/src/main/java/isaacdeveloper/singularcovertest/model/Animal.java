package isaacdeveloper.singularcovertest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Animal implements Parcelable {

    private int id;
    private String name;
    private String kind;
    private String favouriteFood;
    private String comments;
    private String happyMessage;
    private String sadMessage;
    private ArrayList<Animal> friends;
    private int happyPhoto;
    private int sadPhoto;

    public Animal(int id, String name, String kind, String favouriteFood, String comments,
                  String happyMessage, String sadMessage, ArrayList<Animal> friends,
                  int happyPhoto, int sadPhoto) {
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.favouriteFood = favouriteFood;
        this.comments = comments;
        this.happyMessage = happyMessage;
        this.sadMessage = sadMessage;
        this.friends = friends;
        this.happyPhoto = happyPhoto;
        this.sadPhoto = sadPhoto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getFavouriteFood() {
        return favouriteFood;
    }

    public void setFavouriteFood(String favouriteFood) {
        this.favouriteFood = favouriteFood;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getHappyMessage() {
        return happyMessage;
    }

    public void setHappyMessage(String happyMessage) {
        this.happyMessage = happyMessage;
    }

    public String getSadMessage() {
        return sadMessage;
    }

    public void setSadMessage(String sadMessage) {
        this.sadMessage = sadMessage;
    }

    public ArrayList<Animal> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<Animal> friends) {
        this.friends = friends;
    }

    public int getHappyPhoto() {
        return happyPhoto;
    }

    public void setHappyPhoto(int happyPhoto) {
        this.happyPhoto = happyPhoto;
    }

    public int getSadPhoto() {
        return sadPhoto;
    }

    public void setSadPhoto(int sadPhoto) {
        this.sadPhoto = sadPhoto;
    }

    public String getMessageBasedOnMood(){

        if (friends != null) {

            if (friends.size() > 0) {
                return getHappyMessage();
            } else {
                return getSadMessage();
            }

        } else {
            return getSadMessage();
        }
    }

    public int getPhotoBasedOnMood(){

        if (friends != null) {

            if (friends.size() > 0) {
                return getHappyPhoto();
            } else {
                return getSadPhoto();
            }

        } else {
            return getSadPhoto();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.kind);
        dest.writeString(this.favouriteFood);
        dest.writeString(this.comments);
        dest.writeString(this.happyMessage);
        dest.writeString(this.sadMessage);
        dest.writeTypedList(this.friends);
        dest.writeInt(this.happyPhoto);
        dest.writeInt(this.sadPhoto);
    }

    protected Animal(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.kind = in.readString();
        this.favouriteFood = in.readString();
        this.comments = in.readString();
        this.happyMessage = in.readString();
        this.sadMessage = in.readString();
        this.friends = in.createTypedArrayList(Animal.CREATOR);
        this.happyPhoto = in.readInt();
        this.sadPhoto = in.readInt();
    }

    public static final Creator<Animal> CREATOR = new Creator<Animal>() {
        @Override
        public Animal createFromParcel(Parcel source) {
            return new Animal(source);
        }

        @Override
        public Animal[] newArray(int size) {
            return new Animal[size];
        }
    };
}
